#### Intial values.
# bowtie2 index name with path
BOWTIE2_INDEX = "/nobackup/bioinfo_share/software/bowtie2_indexes/mm10"
# output of : samtools faidx mm10.fa
FASTA_FAIDX = "/nobackup/bioinfo_share/software/bowtie2_indexes/mm10.fa.fai"
# Folder in which picard.jar is present
PICARD_HOME = "/nobackup/bioinfo_share/software/picard/dist/"

#### Run-time variables

# Java memory to be used
JAVA_MEM = -Xmx8G
# Read quality filter for trim_galore
readQual = 30
# Alignment quality filter for samtools
alignQual = 20

####  Executables
# Trim-galore executable
TRIM_GALORE = /nobackup/bioinfo_share/software/bin/trim_galore
# Fastqc executable
FASTQC = /nobackup/bioinfo_share/software/bin/fastqc
# samtools executable
SAMTOOLS = /nobackup/bioinfo_share/software/bin/samtools
# bowtie2 executable
ALIGNER = /nobackup/bioinfo_share/software/bin/bowtie2
# Picard executable
PICARD = java -jar $(JAVA_MEM) $(PICARD_HOME)/picard.jar


