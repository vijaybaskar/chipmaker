# Make file
# If you are using picard in HPC. Then remember to load java module.
# Default run: make -f Makefile.xxxx fastqFile="fastqFile.gz" p=4

# Input parameters: 
#### Required Parameters:
# threads: p=4
# fastqFile=fastqfile.gz (or)
# fastqTrimFile=fastqfile_trimmed.fq.gz (or)
# bamFile=fastqfile.bam (or)


include software_parameters.mk

#### Functions
# sets the value of 1st variable as 2nd variable
set = $(eval $1 := $2) 


#### Premade scripts
.dummy: all
all: processFastq alignment processAlign clean
processFastq: getExptName checkFastq fastqc fastqQualFilter clean
alignment: getExptName bowtieAlign clean
processAlign: getExptName bamProcess clean



fastqc: getExptName checkFastq
	rm -rf $(baseFileName).fastqcOutput/
	mkdir $(baseFileName).fastqcOutput/
	$(FASTQC) $(fastqFile) -d ./ -o $(baseFileName).fastqcOutput/ --extract --threads $(p)
	$(eval fastqcFolder := "$(baseFileName).fastqOutput")
	@echo "*****************************************"
	@echo "--> Fastqc Report folder: $(fastqcFolder)"
	@echo "*****************************************"

fastqQualFilter: fastqc
	$(TRIM_GALORE) -q $(readQual) $(fastqFile) 
	$(call set,fastqTrimFile,$(baseFileName)_trimmed.fq.gz) 
	@echo "*****************************************"
	@echo "--> Fastq File: $(fastqTrimFile)"
	@echo "*****************************************"
	

bowtieAlign: checkTrimFastq getExptName
	@echo "**** Fastq File: $(fastqTrimFile)"
	$(eval tmpfile=$(shell mktemp --tmpdir=./))
	@echo "Temp alignment file: $(tmpfile)"
	$(ALIGNER) -p $(p) -x $(BOWTIE2_INDEX) -U $(fastqTrimFile) -S $(tmpfile)
	$(SAMTOOLS) view -bt $(BOWTIE_INDEX).fa.fai $(tmpfile) -o - > $(tmpfile).bam
	$(SAMTOOLS) sort $(tmpfile).bam $(baseFileName)
	$(eval bamFile := "$(baseFileName).bam")
	rm $(tmpfile)*


bamProcess: checkBam getExptName
	$(SAMTOOLS) index $(bamFile)
	$(SAMTOOLS) view -bq $(alignQual) $(bamFile) -o $(baseFileName).mapq20.bam
	$(shell mkdir $(baseFileName).picard/)
	$(eval picardFolder := "$(baseFileName).picard")
	$(PICARD) QualityScoreDistribution INPUT=$(bamFile) OUTPUT=$(picardFolder)/$(baseFileName)_QualityScoreDistribution.txt CHART_OUTPUT=$(picardFolder)/$(baseFileName)_QualityScoreDistribution.pdf TMP_DIR=./ VALIDATION_STRINGENCY=LENIENT
	$(PICARD) CollectAlignmentSummaryMetrics INPUT=$(bamFile) OUTPUT=$(picardFolder)/$(baseFileName)_CollectAlignmentSummaryMetrics.txt TMP_DIR=./ VALIDATION_STRINGENCY=LENIENT
	$(PICARD) MarkDuplicates INPUT=$(bamFile) OUTPUT=$(baseFileName).dupMarked.bam METRICS_FILE=$(picardFolder)/picard_MarkDuplicates_metricsfile.txt REMOVE_DUPLICATES=false TMP_DIR=./ VALIDATION_STRINGENCY=LENIENT
	$(PICARD) MarkDuplicates INPUT=$(bamFile) OUTPUT=$(baseFileName).remDup.bam METRICS_FILE=$(picardFolder)/picard_MarkDuplicates_metricsfile.txt REMOVE_DUPLICATES=true TMP_DIR=./ VALIDATION_STRINGENCY=LENIENT


clean: 
	rm -f _align*

checkFastq:
ifndef fastqFile
	$(error fastqFile not defined)
endif

checkTrimFastq: getExptName
ifndef fastqTrimFile
		$(call set,fastqTrimFile,$(baseFileName)_trimmed.fq.gz)
endif

checkBam: getExptName
ifndef bamFile
	$(call set,bamFile,$(baseFileName).bam)
#	$(error bamFile not defined)
endif


getExptName:
ifdef fastqFile 
	$(eval baseFileName := $(shell echo $(fastqFile) | sed -e 's/.fastq//' -e 's/.fq//' -e 's/.gz//'))
	@echo "*****************************************"
	@echo "Getting basename from fastqFile -->> $(baseFileName)"
	@echo "*****************************************"
else ifdef fastqTrimFile
	$(eval baseFileName := $(shell echo $(fastqTrimFile)  | sed -e 's/.fastq//' -e 's/.fq//' -e 's/.gz//' -e 's/_trimmed//g'))
	@echo "Getting basename from fastqTrimFile -->> $(baseFileName)"
else ifdef bamFile
	$(eval baseFileName := $(shell basename $(bamFile) .bam))
	@echo "*****************************************"
	@echo "Getting basename from bamFile -->> $(baseFileName)"
	@echo "*****************************************"
else 
	$(error make -f Makefile.xxxx fastqFile="fastqFile.gz" p=4)
	
endif
