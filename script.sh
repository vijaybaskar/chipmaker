#!/bin/sh
#$ -cwd
#$ -V
#$ -l h_rt=4:00:00
#$ -l node_type=48core-3T
#$ -pe smp 8
#$ -N makefile

module load java
source ~/.bashrc
export _JAVA_OPTIONS="-Xmx8G"
make -f Makefile -e all fastqFile=ENCFF001ODK.fastq.gz p=8
