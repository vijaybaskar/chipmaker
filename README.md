# Introduction
This is a Makefile pipeline for
1.	QC fastq files (only unpaired supported now)
2.	Trim fastq files (trim_galore, Q >= 20)
3.	Align to a genome (bowtie2)
4.	Sort, mark and remove duplicates (Picard)

# Before running
First get into the respective Makefile and edit the parameters according to your machine conf.

# Main command
The main command is 

	make -f Makefile -e all fastqFile=ENCFF001ODK.fastq.gz p=8

For human genome
	make -f Makefile.hg38.fq2bam -e all fastqFile=ENCFF001ODK.fastq.gz p=8

